﻿using InstituteERP.Core.Data;
using InstituteERP.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstituteERP.Data
{
    public class EfRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly IDbContext _context;
        private IDbSet<TEntity> _entities;

        private EfDatabaseContext db;

        public EfRepository(IDbContext context)
        {
            _context = context;
            db = (EfDatabaseContext) context;
        }


        protected virtual IDbSet<TEntity> Entities
        {
            get 
            {
                if (_entities == null)
                    _entities = _context.Set<TEntity>();
                return _entities;
            }
        }

        public TEntity GetById(object id)
        {
            return Entities.Find(id);
        }

        public void Insert(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            this.Entities.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            this.Entities.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            //todo: finder better way to do this

            if (entity == null)
                throw new ArgumentNullException("entity");

            _context.SaveChanges();
        }

        public IQueryable<TEntity> Table
        {
            get { return Entities; }
        }

        public IList<TEntity> ExecuteStoredProcedureList<TEntity>(string command, params object[] parameters) where TEntity : BaseEntity, new()
        {
            return _context.ExecuteStoredProcedureList<TEntity>(command, parameters);
        }
    }
}
