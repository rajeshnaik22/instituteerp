﻿using InstituteERP.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstituteERP.Data
{
    public class DatabaseInitializer : DropCreateDatabaseIfModelChanges<EfDatabaseContext>
    {
        protected override void Seed(EfDatabaseContext context)
        {
            for(int i=0; i<10 ; i++)
            {
                User user = new User 
                {
                    Email=string.Format("User{0}@ya.com",i),
                    Password="1234",
                    FirstName="User"+i,
                    LastName="UserLastName"+i
                };

                context.Set<User>().Add(user);
            }
            context.SaveChanges();


            Role role1 = new Role { Name = "Admin" };
            Role role2 = new Role { Name = "Student" };
            Role role3 = new Role { Name = "Teacher" };

            context.Set<Role>().Add(role1);
            context.Set<Role>().Add(role2);
            context.Set<Role>().Add(role3);
            context.SaveChanges();



            Random rnd = new Random();

            for (int i = 0; i < 10; i++)
            {
                UserRole userRole = new UserRole 
                {
                    UserId=rnd.Next(1,10), //ids in database starts from 1, not 0
                    RoleId=rnd.Next(1,3)
                };

                context.Set<UserRole>().Add(userRole);
            }
            context.SaveChanges();


            for (int i = 0; i < 10; i++)
            {
                Course course = new Course { Name = "Software Course " + 1 };
                context.Set<Course>().Add(course);
            }
            context.SaveChanges();
        }
    }
}
