﻿using InstituteERP.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstituteERP.Data
{
    public interface IDbContext
    {
        IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity;

        int SaveChanges();

        IList<TEntity> ExecuteStoredProcedureList<TEntity>(string command, params object[] parameters)
            where TEntity : BaseEntity, new();


        IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters);


        int ExecuteSqlCommand(string sql, bool doNotEnsureTransaction = false, int? timeout = null, params object[] parameters);

        /// <summary>
        /// Detach an entity
        /// </summary>
        /// <param name="entity"></param>
        void Detach(object entity);

        bool AutoDetectChangesEnabled { get; set; }
    }
}
