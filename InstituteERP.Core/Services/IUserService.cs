﻿using InstituteERP.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstituteERP.Core.Services
{
    public interface IUserService 
    {
        void AddUser(User user);
        void DeleteUser(User user);
        void UpdateUser(User user);

        User GetUserById(int id);
        User GetUserByEmail(string email);

        IList<User> GetAll();

        bool ValidateCredentials(string email, string password);
    }
}
