﻿using InstituteERP.Core.Data;
using InstituteERP.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstituteERP.Core.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _userRepository;

        public UserService(IRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }

        public void AddUser(Entities.User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            _userRepository.Insert(user);

        }

        public void DeleteUser(Entities.User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            _userRepository.Delete(user);
        }

        public void UpdateUser(Entities.User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            _userRepository.Update(user);
        }

        public Entities.User GetUserById(int id)
        {
            return _userRepository.GetById(id);
        }

        public Entities.User GetUserByEmail(string email)
        {
            return _userRepository.Table.FirstOrDefault(u => u.Email == email);
        }

        public IList<Entities.User> GetAll()
        {
            return _userRepository.Table.ToList();
        }

        public bool ValidateCredentials(string email, string password)
        {
            if (string.IsNullOrWhiteSpace(email)
                || string.IsNullOrWhiteSpace(password))
                return false;

            return _userRepository.Table.FirstOrDefault(u => u.Email == email && u.Password == password) != null;
        }
    }
}
