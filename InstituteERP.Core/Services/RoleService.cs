﻿using InstituteERP.Core.Data;
using InstituteERP.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstituteERP.Core.Services
{
    public class RoleService : IRoleService
    {
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<UserRole> _userRoleRepository;
        public RoleService(
            IRepository<Role> roleRepository, 
            IRepository<User> userRepository,
            IRepository<UserRole> userRoleRepository
            )
        {
            _roleRepository = roleRepository;
            _userRepository = userRepository;
            _userRoleRepository = userRoleRepository;
        }



        public void Add(Role role)
        {
            if (role == null)
                throw new ArgumentNullException("Role");
            _roleRepository.Insert(role);

        }

        public void Delete(Role role)
        {
            if (role == null)
                throw new ArgumentNullException("Role");
            _roleRepository.Delete(role);
        }

        public void Update(Role role)
        {
            if (role == null)
                throw new ArgumentNullException("Role");

            _roleRepository.Update(role);
        }

        public IList<Role> GetAll()
        {
            return _roleRepository.Table.ToList();
        }

        public IList<Role> GetRolesForUser(string username)
        {
            List<Role> roles = new List<Role>();
            var user=_userRepository.Table.FirstOrDefault(u => u.Email == username);
            if (user == null)
                return roles;

            var query = from m in _userRoleRepository.Table
                    join r in _roleRepository.Table on m.RoleId equals r.Id
                    where m.UserId == user.Id
                    select r;

            return query.ToList();

        }

        public bool IsUserInRole(string username, string role)
        {
            var userRoles = GetRolesForUser(username);
            return userRoles.FirstOrDefault(r => r.Name == role) != null;
        }
    }
}
