﻿using InstituteERP.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstituteERP.Core.Services
{
    public interface IService<TEntity> where TEntity : BaseEntity
    {
        void AddUser(TEntity entity);
        void DeleteUser(TEntity entity);
        void UpdateUser(TEntity entity);

        TEntity GetById(int id);

        IList<TEntity> GetAll();

    }
}
