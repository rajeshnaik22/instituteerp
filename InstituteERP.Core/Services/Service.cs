﻿using InstituteERP.Core.Data;
using InstituteERP.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstituteERP.Core.Services
{
    public class Service<TEntity> : IService<TEntity> where TEntity : BaseEntity
    {
        private readonly IRepository<TEntity> _entityRepository;

        public Service(IRepository<TEntity> entityRepository)
        {
            _entityRepository = entityRepository;
        }

        public void AddUser(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");
            _entityRepository.Insert(entity);

        }

        public void DeleteUser(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");
            _entityRepository.Delete(entity);

        }

        public void UpdateUser(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            _entityRepository.Update(entity);

        }

        public TEntity GetById(int id)
        {
            return _entityRepository.GetById(id);
        }

        public IList<TEntity> GetAll()
        {
            return _entityRepository.Table.ToList();
        }
    }
}
