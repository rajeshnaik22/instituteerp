﻿using InstituteERP.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstituteERP.Core.Services
{
    public interface IRoleService
    {
        void Add(Role role);
        void Delete(Role role);
        void Update(Role role);

        IList<Role> GetAll();

        IList<Role> GetRolesForUser(string username);

        bool IsUserInRole(string username, string role);
    }
}
