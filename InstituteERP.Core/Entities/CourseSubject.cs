﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstituteERP.Core.Entities
{
    public class CourseSubject : BaseEntity
    {
        public int CourseId { get; set; }

        public int SubjectId { get; set; }
    }
}
