﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstituteERP.Core.Entities
{
    public class Course : BaseEntity
    {
        public Course()
        {
            Name += DateTime.Now.Second.ToString();
        }

        [Required]
        public string Name { get; set; }

    }
}
