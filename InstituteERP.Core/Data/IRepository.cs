﻿using InstituteERP.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstituteERP.Core.Data
{
    public interface IRepository<T> where T:BaseEntity
    {
        T GetById(object id);

        void Insert(T entity);

        void Delete(T entity);

        void Update(T entity);

        IQueryable<T> Table { get; }

        IList<TEntity> ExecuteStoredProcedureList<TEntity>(string command, params object[] parameters)
            where TEntity : BaseEntity, new();
    }
}
