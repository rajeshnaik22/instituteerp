﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using InstituteERP.Core.Entities;
using InstituteERP.Core.Services;

namespace InstituteERP.Web.Controllers
{
    public class CourseController : Controller
    {
	    IService<Course> _courseService;

		public CourseController(IService<Course> service)
        {
            _courseService = service;
        }


        // GET: Course
        public ActionResult Index()
        {
            return View();
        }

		public ActionResult ListPartial(int? page, string searchText)
        {
            ViewBag.SearchText = searchText;
            List<Course> items = _courseService.GetAll().ToList(); 
            if(!string.IsNullOrWhiteSpace(searchText))
                items=items.Where(c=>c.Name.IndexOf(searchText,StringComparison.OrdinalIgnoreCase)>=0).ToList();

            int currentPage = page.HasValue ? page.Value : 1;
            return PartialView(items.ToPagedList(currentPage, 10));
        }


        // GET: Course/Details/5
        public ActionResult Details(int id)
		{
			var item =_courseService.GetById(id);

            return PartialView(item);
        }

        // GET: Course/Create
        public ActionResult Create()
        {
			Course item = new Course();
			if (Request.IsAjaxRequest())
                return PartialView("CreateOrEdit",item);
            else
                return View("CreateOrEdit",item);
        }

        // POST: Course/Create
        [HttpPost]
        public ActionResult Create(Course item,FormCollection collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _courseService.AddUser(item);
                    return RedirectToAction("ListPartial");
                }
                else
                {
                    this.Response.StatusCode = 400;
                    return PartialView("CreateOrEdit",item);
                }
            }
            catch
            {
                return PartialView("CreateOrEdit",item);
            }
        }

        // GET: Course/Edit/5
        public ActionResult Edit(int id)
        {
			var item = _courseService.GetById(id);
            return PartialView("CreateOrEdit", item);
        }

        // POST: Course/Edit/5
        [HttpPost]
        public ActionResult Edit(Course item, FormCollection collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var itemDb = _courseService.GetById(item.Id);
                    /* add here required properties
					itemDb.Name = item.Name;

					*/
                    _courseService.UpdateUser(itemDb);
                    return RedirectToAction("ListPartial");
                }
                return View("CreateOrEdit", item);
            }
            catch
            {
                return View("CreateOrEdit", item); 
            }
        }

        // GET: Course/Delete/5
        public ActionResult Delete(int id)
        {
		    var item = _courseService.GetById(id);
            return PartialView(item);
        }

        // POST: Course/Delete/5
        [HttpPost]
        public ActionResult Delete(Course item, FormCollection collection)
        {
            try
            {
                var itemDb = _courseService.GetById(item.Id);
                _courseService.DeleteUser(itemDb);
                return RedirectToAction("ListPartial");
            }
            catch
            {
                return PartialView(item);
            }
        }
    }
}
