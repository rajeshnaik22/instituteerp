﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InstituteERP.Core.Entities;
using System.Web.Security;

namespace InstituteERP.Web.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Login()
        {
            User user = new User();

            return View(user);
        }

        [HttpPost]
        public ActionResult Login(User user, string returnUrl)
        {
            ModelState["FirstName"].Errors.Clear();
            ModelState["LastName"].Errors.Clear();

            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(user.Email, user.Password))
                {
                    FormsAuthentication.SetAuthCookie(user.Email, false);
                    if (!string.IsNullOrWhiteSpace(returnUrl))
                        return Redirect(returnUrl);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "The email id or password provided is incorrect");
                }
            }
            return View(user);
        }


        

    }
}