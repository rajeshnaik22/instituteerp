﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using InstituteERP.Core.Entities;
using InstituteERP.Core.Services;

namespace InstituteERP.Web.Controllers
{
    public class SubjectController : Controller
    {
	    IService<Subject> _subjectService;

		public SubjectController(IService<Subject> service)
        {
            _subjectService = service;
        }


        // GET: Subject
        public ActionResult Index()
        {
            return View();
        }

		public ActionResult ListPartial(int? page, string searchText)
        {
            ViewBag.SearchText = searchText;
            List<Subject> items = _subjectService.GetAll().ToList(); 
            if(!string.IsNullOrWhiteSpace(searchText))
                items=items.Where(c=>c.Name.IndexOf(searchText,StringComparison.OrdinalIgnoreCase)>=0).ToList();

            int currentPage = page.HasValue ? page.Value : 1;
            return PartialView(items.ToPagedList(currentPage, 10));
        }


        // GET: Subject/Details/5
        public ActionResult Details(int id)
		{
			var item =_subjectService.GetById(id);

            return PartialView(item);
        }

        // GET: Subject/Create
        public ActionResult Create()
        {
            ViewBag.Edit = false;
			Subject item = new Subject();
			if (Request.IsAjaxRequest())
                return PartialView("CreateOrEdit",item);
            else
                return View("CreateOrEdit",item);
        }

        // POST: Subject/Create
        [HttpPost]
        public ActionResult Create(Subject item,FormCollection collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _subjectService.AddUser(item);
                    return RedirectToAction("ListPartial");
                }
                else
                {
                    this.Response.StatusCode = 400;
                    return PartialView("CreateOrEdit",item);
                }
            }
            catch (Exception ex)
            {
			    this.Response.StatusCode = 400;
                ModelState.AddModelError("","Error:" + ex.Message);

                return PartialView("CreateOrEdit",item);
            }
        }

        // GET: Subject/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.Edit = true;
			var item = _subjectService.GetById(id);
            return PartialView("CreateOrEdit", item);
        }

        // POST: Subject/Edit/5
        [HttpPost]
        public ActionResult Edit(Subject item, FormCollection collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var itemDb = _subjectService.GetById(item.Id);
                    throw new NotImplementedException();
					/* add here required properties
					itemDb.Name = item.Name;

					*/
                    _subjectService.UpdateUser(itemDb);
                    return RedirectToAction("ListPartial");
                }

				this.Response.StatusCode = 400;
                ModelState.AddModelError("", "Please correct following errors:");

                return PartialView("CreateOrEdit", item);

            }
            catch (Exception ex)
            {
			    this.Response.StatusCode = 400;
                ModelState.AddModelError("","Error:" + ex.Message);

                return PartialView("CreateOrEdit", item); 
            }
        }

        // GET: Subject/Delete/5
        public ActionResult Delete(int id)
        {
		    var item = _subjectService.GetById(id);
            return PartialView(item);
        }

        // POST: Subject/Delete/5
        [HttpPost]
        public ActionResult Delete(Subject item, FormCollection collection)
        {
            try
            {
                var itemDb = _subjectService.GetById(item.Id);
                _subjectService.DeleteUser(itemDb);
                return RedirectToAction("ListPartial");
            }
            catch(Exception ex)
            {
			    this.Response.StatusCode = 400;
                ModelState.AddModelError("","Error:" + ex.Message);

                return PartialView(item);
            }
        }
    }
}
