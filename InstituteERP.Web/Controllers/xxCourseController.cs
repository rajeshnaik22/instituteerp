﻿using InstituteERP.Core.Entities;
using InstituteERP.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace InstituteERP.Web.Controllers
{
    public class xxCourseController : Controller
    {
        IService<Course> _courseService;

        public xxCourseController(IService<Course> service)
        {
            _courseService = service;
        }

        // GET: Course
        public ActionResult Index(int? page)
        {
            List<Course> courses = _courseService.GetAll().ToList();

            int currentPage = page.HasValue ? page.Value : 1;
            return View(courses.ToPagedList(currentPage, 10));
        }



        public ActionResult ListPartial(int? page, string searchText)
        {
            ViewBag.SearchText = searchText;
            List<Course> courses = _courseService.GetAll().ToList(); 
            if(!string.IsNullOrWhiteSpace(searchText))
                courses=courses.Where(c=>c.Name.IndexOf(searchText,StringComparison.OrdinalIgnoreCase)>=0).ToList();

            int currentPage = page.HasValue ? page.Value : 1;
            return PartialView(courses.ToPagedList(currentPage, 10));
        }

        // GET: Course/Details/5
        public ActionResult Details(int id)
        {
            var course = _courseService.GetById(id);
            return PartialView(course);
        }

        // GET: Course/Create
        public ActionResult Create()
        {
            Course course = new Course();
            if (Request.IsAjaxRequest())
                return PartialView("CreateOrEdit",course);
            else
                return View("CreateOrEdit",course);
        }

        // POST: Course/Create
        [HttpPost]
        public ActionResult Create(Course course)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _courseService.AddUser(course);
                    return RedirectToAction("ListPartial");
                }
                else
                {
                    this.Response.StatusCode = 400;
                    return PartialView("CreateOrEdit",course);
                }
            }
            catch
            {
                return PartialView("CreateOrEdit",course);
            }
        }

        // GET: Course/Edit/5
        public ActionResult Edit(int id)
        {
            var course = _courseService.GetById(id);
                      
                return PartialView("CreateOrEdit", course);
        }

        // POST: Course/Edit/5
        [HttpPost]
        public ActionResult Edit(Course course)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var courseDb = _courseService.GetById(course.Id);
                    courseDb.Name = course.Name;

                    _courseService.UpdateUser(courseDb);
                    return RedirectToAction("ListPartial");
                }
                return View("CreateOrEdit", course);
            }
            catch
            {
                return View("CreateOrEdit", course); 
            }
        }

        // GET: Course/Delete/5
        public ActionResult Delete(int id)
        {
            var course = _courseService.GetById(id);
            return PartialView(course);
        }

        // POST: Course/Delete/5
        [HttpPost]
        public ActionResult Delete(Course course, FormCollection collection)
        {
            try
            {
                var courseDb = _courseService.GetById(course.Id);
                _courseService.DeleteUser(courseDb);
                return RedirectToAction("ListPartial");
            }
            catch
            {
                return PartialView(course);
            }
        }
    }
}
